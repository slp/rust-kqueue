# Changelog

## 1.0.2

* Fixed #4: Fix bug where wrong data types were used on i386 FreeBSD

## 1.0.1

* Merged !1 as a fix for #3. We properly fill in the `ext` field for `kqueue`
  extensions on FreeBSD.

## 1.0.0

### Breaking changes

* Bumped `bitflags` in `rust-kqueue-sys`: Now all bitflag constants must be qualified:

`EV_DELETE` -> `EventFlag::EV_DELETE`
`NOTE_WRITE` > `FilterFlag::NOT_WRITE`

### Other changes

* 2018 edition and clippy changes
